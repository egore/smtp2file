/*
 * Copyright 2014-2015  Christoph Brill <egore911@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.egore911.smtp2file;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.URLName;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Fake SMTP provider that saves e-mails to a directory instead of sending them.
 * It will misuse the SMTP host to specify the directory.
 * 
 * @author Christoph Brill &lt;egore911@gmail.com&gt;
 */
public class SMTPTransport extends Transport {

	/** Directory to save the emails to. */
	private File dir;
	private final InternetAddress forceTo;

	public static File lastFile;

	/**
	 * Constructor.
	 * 
	 * @param session
	 *            Session object for this Transport.
	 * @param urlname
	 *            URLName object to be used for this Transport
	 */
	public SMTPTransport(Session session, URLName urlname) {
		super(session, urlname);
		InternetAddress force = null;
		try {
			String address = (String) session.getProperties().get(
					"mail.smtp.force_to");
			if (address != null) {
				force = new InternetAddress(address);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.forceTo = force;
	}

	/**
	 * Perform a fake connect by checking if whatever is set in "mail.smtp.host"
	 * (and passed as the parameter host here) is a directory (or could be
	 * created as directory). If that is the case the connect was successful.
	 */
	@Override
	protected boolean protocolConnect(String host, int port, String user,
			String password) throws MessagingException {

		// check if a host was given
		if (host == null || host.isEmpty()) {
			throw new MessagingException(
					"System property \"mail.smtp.host\" is not set");
		}

		File possibleDir = new File(host);

		// if the directory does not exist yet, try to create it
		if (!possibleDir.exists() && !possibleDir.mkdirs()) {
				throw new MessagingException(possibleDir
						+ " does not exist and could not be created");

		}

		// check if the given path points to directory
		if (!possibleDir.isDirectory()) {
			throw new MessagingException(possibleDir + " is not a directory");
		}

		// check if the directory is writable
		if (!possibleDir.canWrite()) {
			throw new MessagingException(possibleDir + " is not writable");
		}

		// At this point everything should be set up to save emails
		this.dir = possibleDir;
		return isConnected();
	}

	/**
	 * @return <code>true</code>, if the system property "mail.smtp.host" points
	 *         to a valid directory.
	 */
	@Override
	public synchronized boolean isConnected() {
		return this.dir != null;
	}

	/**
	 * Save the given message to a file.
	 */
	@Override
	public void sendMessage(Message message, Address[] addresses)
			throws MessagingException {
		try {
			String messageId;
			if (message instanceof MimeMessage) {
				// a MessageID only exists for MimeMessage
				MimeMessage mimeMessage = (MimeMessage) message;
				messageId = mimeMessage.getMessageID();
			} else {
				// fallback that should never be triggered, just use something
				// unique
				messageId = UUID.randomUUID().toString();
			}
			if (forceTo != null) {
				message.setRecipient(Message.RecipientType.TO, forceTo);
			}

			// save the file to configured directory
			File f = new File(dir, clean(messageId) + ".eml");
            try (FileOutputStream out = new FileOutputStream(f)) {
                message.writeTo(out);
            }
			lastFile = f;
		} catch (IOException e) {
			throw new MessagingException(e.getMessage(), e);
		}
	}

	/**
	 * Replace anything that is not an alphabetic char and not a number by an
	 * underscore. This is done to get a safe filename.
	 * 
	 * @param messageId
	 *            string to be cleaned up
	 * @return string consisting only of alphabetic chars and numbers
	 */
	private static String clean(String messageId) {
		return messageId.replaceAll("\\W", "_").replaceAll("^_+|_+$", "");
	}
}
