/*
 * Copyright 2014-2015  Christoph Brill <egore911@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.egore911.smtp2file;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import java.io.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

/**
 * @author Christoph Brill &lt;egore911@gmail.com&gt;
 */
class MailTest {

	@BeforeEach
	public void beforeEach() {
		SMTPTransport.lastFile = null;
	}

	@AfterEach
	public void afterEach() {
		if (SMTPTransport.lastFile != null) {
			SMTPTransport.lastFile.delete();
		}
	}

	@Test
	void testHappyPath() throws MessagingException, IOException {
		System.setProperty("mail.smtp.host", "target");
		System.clearProperty("mail.smtp.force_to");
		Session session = Session.getDefaultInstance(System.getProperties());

        sendMessage(session);

        assertThat(SMTPTransport.lastFile).exists();
		try (BufferedReader reader = new BufferedReader(new FileReader(SMTPTransport.lastFile))) {
			assertThat(reader.readLine()).isEqualTo("From: from@address.local");
			assertThat(reader.readLine()).isEqualTo("To: to@address.local");
		}
	}

	@Test
	void testFileInvalid() {
		System.setProperty("mail.smtp.host", "pom.xml");
		System.clearProperty("mail.smtp.force_to");
		Session session = Session.getDefaultInstance(System.getProperties());

		assertThatThrownBy(() -> sendMessage(session))
				.isInstanceOf(MessagingException.class)
				.hasMessage("pom.xml is not a directory");

		assertThat(SMTPTransport.lastFile).isNull();
	}

	@Test
	@Disabled("If run as root, creating '/bla' would succeed")
	void testDirInvalid() {
		System.setProperty("mail.smtp.host", "/bla");
		System.clearProperty("mail.smtp.force_to");
		Session session = Session.getDefaultInstance(System.getProperties());

		assertThatThrownBy(() -> sendMessage(session))
				.isInstanceOf(MessagingException.class)
				.hasMessage("/bla does not exist and could not be created");

		assertThat(SMTPTransport.lastFile).isNull();
	}

	@Test
	void testForceTo() throws MessagingException, IOException {
		System.setProperty("mail.smtp.host", "target");
		System.setProperty("mail.smtp.force_to", "noone@nowhere.none");
		Session session = Session.getDefaultInstance(System.getProperties());

		sendMessage(session);

		assertThat(SMTPTransport.lastFile).exists();
		try (BufferedReader reader = new BufferedReader(new FileReader(SMTPTransport.lastFile))) {
			assertThat(reader.readLine()).isEqualTo("From: from@address.local");
			assertThat(reader.readLine()).isEqualTo("To: noone@nowhere.none");
		}
	}

	private static void sendMessage(Session session) throws MessagingException {
		MimeMessage mimeMessage = new MimeMessage(session);
		mimeMessage.setFrom(new InternetAddress("from@address.local"));
		mimeMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(
				"to@address.local"));
		mimeMessage.setSubject("My subject");
		mimeMessage.setText("My body");
		Transport.send(mimeMessage);
	}
}