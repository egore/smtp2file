## [1.0.6](https://gitlab.com/appframework/egore-personal/smtp2file/compare/1.0.5...1.0.6) (2025-02-26)

## [1.0.5](https://gitlab.com/appframework/egore-personal/smtp2file/compare/1.0.4...1.0.5) (2025-01-22)

## [1.0.4](https://gitlab.com/appframework/egore-personal/smtp2file/compare/1.0.3...1.0.4) (2025-01-08)

## [1.0.3](https://gitlab.com/appframework/egore-personal/smtp2file/compare/1.0.2...1.0.3) (2024-12-25)

## [1.0.2](https://gitlab.com/appframework/egore-personal/smtp2file/compare/1.0.1...1.0.2) (2024-12-18)

## [1.0.1](https://gitlab.com/appframework/egore-personal/smtp2file/compare/v1.0.0...1.0.1) (2024-10-22)

# 1.0.0 (2024-10-09)
