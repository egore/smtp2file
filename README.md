smtp2file
=========

*smtp2file* is a fake SMTP provider for the Java mail API. It will store the emails
to a directory instead of actually sending them. 

Setup
-----

For a complete setup of *smtp2file* you only need to perform two operations:

1. add smtp2file to the classpath of your application (or application server)
2. set the system property "mail.smtp.host" to point to a directory

Used software
-------------

The following components were used to develop *smtp2file*:

* Java mail API 1.4
* junit 4.11
